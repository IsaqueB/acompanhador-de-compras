const express = require('express')

const Purchase = require('../models/purchases')

const router = express.Router()

router.post('/new_purchase', async (req, res) =>{
    try{
        console.log(req.body)
        const purchase = await Purchase.create(req.body)

        return res.status(200).send()
    }
    catch(err){
        console.log(err);
        return res.status(400).send({ error: 'Failed to add your new purchase' })
    }
})

router.get('/list_all', async (req,res) => {
    let all_purchases = {}
    try{
        all_purchases = await Purchase.find({})
    } catch (err){
        res.status(400).send({error:"sei la porran"})
    }
    res.send({all_purchases})
})

router.get('/ok', async (req,res) => {
    res.send("ok")
})


module.exports = server => server.use(router)